﻿using UnityEngine;
using System.Collections.Generic;

public class Monster : MonoBehaviour {

	public List<Level> level; //Lists Level of wave

	public float Health; //Current Health
	public float Speed;  //Current Speed
	public int KillGold = 10;     //Reward

	public bool Flying;  //Checks if the monster is flying or not

	public int WaveLevel; //Current Wave Level

	private NavMeshAgent monster;
	private GameObject GoldAccumulated; //available Gold of Player

	// Use this for initialization
	void Start () {
		Health = this.level [this.WaveLevel].MonsterHealth; //HP of Monster
		Speed = this.level [this.WaveLevel].MonsterSpeed; //Speed of Monster
		monster = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Health <= 0){
			//Gold.GetComponent<Gold>().GoldReward (this.KillGold);
			UnityEngine.Object.Destroy (base.gameObject);
		}
	}
}