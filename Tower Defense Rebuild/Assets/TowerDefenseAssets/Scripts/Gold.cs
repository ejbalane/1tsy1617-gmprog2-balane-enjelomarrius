﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Gold : MonoBehaviour {
	private Text text;
	private int PlayerGold;
	// Use this for initialization
	void Start () {
		text = gameObject.GetComponent<Text> ();
		text.text = "Money: " + PlayerGold;
	}
	
	// Update is called once per frame
	void Update () {
		text.text = "Player:" + PlayerGold;
	}
	public int GoldReward(int gold){
		this.PlayerGold += gold;
		return this.PlayerGold;
	}
}
