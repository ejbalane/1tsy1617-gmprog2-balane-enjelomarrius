﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {
	public float cameraSpeed = 5;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.A)) {
			transform.position += Vector3.right * cameraSpeed * Time.deltaTime;
		}
		if (Input.GetKey (KeyCode.D)) {
			transform.position += Vector3.left * cameraSpeed * Time.deltaTime;
		}
		if (Input.GetKey (KeyCode.W)) {
			transform.position += Vector3.back * cameraSpeed * Time.deltaTime;
		}
		if (Input.GetKey (KeyCode.S)) {
			transform.position += Vector3.forward * cameraSpeed *Time.deltaTime;
		}
	}
}
