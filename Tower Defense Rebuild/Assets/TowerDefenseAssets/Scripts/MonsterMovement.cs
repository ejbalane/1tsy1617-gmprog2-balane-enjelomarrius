﻿using UnityEngine;
using System.Collections;

public class MonsterMovement : MonoBehaviour {
	public Transform Goal;
	private NavMeshAgent Monster;
	// Use this for initialization
	void Start () {
		Monster = GetComponent<NavMeshAgent> ();
		Monster.SetDestination (Goal.position);
	}
	
	// Update is called once per frame
	void Update () {
		if(Vector3.Distance(this.Goal.position, transform.position) < 1.0f){
			Destroy(base.gameObject);
		}
	}
}
