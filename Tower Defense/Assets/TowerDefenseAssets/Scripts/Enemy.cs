﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	public Transform Goal;
	private NavMeshAgent agent;
	// Use this for initialization
	void Start () {
		agent = gameObject.GetComponent<NavMeshAgent> ();
		agent.SetDestination (Goal.position);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
