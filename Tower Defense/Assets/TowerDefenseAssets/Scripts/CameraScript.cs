﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {
	public float Speed = 10;
	private Camera mainCamera;
	private Vector3 CameraPosition = new Vector3 (28f, 15f, -5f);
	// Use this for initialization
	void Start () {
		this.mainCamera = Camera.main;
		this.mainCamera.transform.position = this.CameraPosition;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.A)) {
			this.CameraPosition.x = this.CameraPosition.x - this.Speed;
		}
		if (Input.GetKey (KeyCode.D)) {
			this.CameraPosition.x = this.CameraPosition.x + this.Speed;
		}
		if (Input.GetKey (KeyCode.S)) {
			this.CameraPosition.z = this.CameraPosition.z - this.Speed;
		}
		if (Input.GetKey (KeyCode.W)) {
			this.CameraPosition.z = this.CameraPosition.z + this.Speed;
		}
		this.mainCamera.transform.position = this.CameraPosition;
	}
}
