﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
	public float PlayerSpeed = 5;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 GotoPoint = Camera.main.ScreenToWorldPoint (Input.mousePosition);

		GotoPoint.z = transform.position.z;

		transform.position = Vector3.MoveTowards (transform.position, GotoPoint, PlayerSpeed * Time.deltaTime/transform.localScale.x);

	}
}
