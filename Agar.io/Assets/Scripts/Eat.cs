﻿using UnityEngine;
using System.Collections;

public class Eat : MonoBehaviour {
	public string Tag;
	public string EnemyTag;
	public float Add;
	//SphereCollider Player = Transform.GetComponent<SphereCollider>().attachedRigidbody;
	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == Tag) {
			transform.localScale += new Vector3 (Add, Add, Add);
			//SphereCollider.transform.localScale += new Vector3 (Add, Add, Add);
			Destroy(other.gameObject);
		}
		if (other.gameObject.tag == EnemyTag) {
			transform.localScale += new Vector3 (Add, Add, Add);
			Destroy (other.gameObject);
		} else {
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
