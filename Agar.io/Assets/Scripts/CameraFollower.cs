﻿using UnityEngine;
using System.Collections;

public class CameraFollower : MonoBehaviour {
	public Transform player;
	public Vector3 CameraOffset;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//Camera follows Player with additional offset
		transform.position = new Vector3 (player.position.x + CameraOffset.x, player.position.y + CameraOffset.y, CameraOffset.z);
	}
}
