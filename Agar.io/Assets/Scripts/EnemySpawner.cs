﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {
	public GameObject EnemyPrefab;
	public float speed;

	void SpawnEnemy(){
		int x = Random.Range (0, Camera.main.pixelWidth);
		int y = Random.Range (0, Camera.main.pixelHeight);

		Vector3 target = Camera.main.ScreenToWorldPoint(new Vector3(x,y,0));
		target.z = 0;

		Instantiate (EnemyPrefab, target, Quaternion.identity);
	}
	// Use this for initialization
	void Start () {
		InvokeRepeating ("SpawnEnemy", 0, speed);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
