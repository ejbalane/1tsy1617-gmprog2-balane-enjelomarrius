﻿using UnityEngine;
using System.Collections;

public class FoodSpawner : MonoBehaviour {
	public GameObject Foodprefab;
	public float velocity;

	void GenerateFood(){
		int x = Random.Range (0, Camera.main.pixelWidth);
		int y = Random.Range (0, Camera.main.pixelHeight);

		Vector3 Target = Camera.main.ScreenToWorldPoint (new Vector3(x,y,0));
		Target.z = 0;

		Instantiate (Foodprefab, Target, Quaternion.identity);
	}
	// Use this for initialization
	void Start () {
		InvokeRepeating ("GenerateFood", 0, velocity);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
