﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ColorPicker : MonoBehaviour {
	public List<Material> Materials = new List<Material>();

	void Awake(){
		//called once only
		GetComponent<Renderer>().material = Materials[Random.Range(0,Materials.Count)];
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
