﻿using System.Linq;
using UnityEngine;
using System.Collections;


public enum States{
	Patrol,
	Chase,
	Eat,
	Run,
	Dead,
}
public class FiniteStateMachine : MonoBehaviour {
	private GameObject player;
	private GameObject closestPlayer;
	private GameObject closestFood;
	public States state = States.Patrol;
	private Transform target;
	private float velocity = 3;
	private Vector3 random;
	public float MinimumDistance = 10f;
	public float RunSpeed = 4;
	void Awake(){
	}
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	void Patrol(){
		Debug.Log ("Patrol");
		Debug.Log (Vector3.Distance (this.transform.position, random));
		if (Vector3.Distance (this.transform.position, random) < 2) {
			float X = Random.Range (-20, 20);
			float Y = Random.Range (-20, 20);
			random = new Vector3 (X, Y, 0);
		}
		this.transform.position = Vector3.MoveTowards (this.transform.position, random, Time.deltaTime);


		if (target) {
			state = States.Chase;
		}
	}
	void Chase(){
		Debug.Log ("Chase");
		if (Vector3.Distance (this.transform.position, target.transform.position) > 5) {
			target = null;
			state = States.Patrol;
		} else {
			this.transform.position = Vector3.MoveTowards (this.transform.position, target.transform.position, Time.deltaTime * velocity);
		}
	}
	void Eat(){
		//Move Towards Food
		Debug.Log("Eat");
		//this.transform.position = Vector3.MoveTowards(this.transform.position, GameObject.FindGameObjectWithTag("Food").transform.position, Time.deltaTime*velocity);

		//Method 1
		/*this.closestFood = (from go in GameObject.FindGameObjectsWithTag ("Food")
		                    orderby Vector3.Distance(go.transform.position, base.transform.position)
		                    select go).FirstOrDefault<GameObject> ();
		*/
		//Method 2
		closestFood = GameObject.FindGameObjectsWithTag("Food").OrderBy
			(go=> Vector3.Distance(go.transform.position, transform.position))
			.FirstOrDefault();

	}
	void Run(){
		//Run Away to the Detected enemy bigger than you

	}
	void Dead(){
		//Dead
		//Destroy when Player Eats.
		Destroy(this.gameObject);
	}
	// Update is called once per frame
	void Update () {
		switch (state) {
		case States.Patrol:
			Patrol ();
			break;
		case States.Chase:
			Chase ();
			break;
		case States.Eat:
			Eat ();
			break;
		case States.Run:
			Run ();
			break;
		case States.Dead:
			Dead ();
			break;
		}
		if (GetComponentInChildren<EnemyDetection> ().otherEnemy != null)
			target = GetComponentInChildren<EnemyDetection> ().otherEnemy.transform;
		if (player.transform.localScale.magnitude <= this.transform.localScale.magnitude) {
			Chase ();
		} else
			Run ();

	}
}
