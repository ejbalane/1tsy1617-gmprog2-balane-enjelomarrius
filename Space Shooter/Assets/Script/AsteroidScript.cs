﻿using UnityEngine;
using System.Collections;

public class AsteroidScript : MonoBehaviour {
	public float fallspeed = 10.0f;
	public float spinspeed = 50.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.back * fallspeed * Time.deltaTime, Space.World);
		transform.Rotate (Vector3.forward, spinspeed * Time.deltaTime);
	}
}
