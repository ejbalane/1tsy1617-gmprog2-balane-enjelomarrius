﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour {
	public float bulletspeed = 50;
	public Rigidbody bullet;
	// Use this for initialization
	void Start () {

	}
	void Fire(){
		Rigidbody bulletClone = (Rigidbody)Instantiate (bullet, transform.position, transform.rotation);
		bulletClone.velocity = transform.forward * bulletspeed;
	}	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space))
			Fire();
	}
}
