﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {
	public GameObject Asteroid;
	public float spawnTime = 3f;
	public Transform[] spawnPoints;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("Spawn", spawnTime, spawnTime);
	}
	
	// Update is called once per frame
	void Update () {
		int SpawnPointIndex = Random.Range (0, spawnPoints.Length);

		Instantiate (Asteroid, spawnPoints [SpawnPointIndex].position, spawnPoints [SpawnPointIndex].rotation);
	}
}
